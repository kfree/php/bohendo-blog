
##### VARIABLES #####

me=$(shell whoami)
v=latest

SHELL=/bin/bash # default: /bin/sh

$(shell mkdir -p .makeflags)
VPATH=.makeflags

wordpressInclude=$(shell find wordpress -type f)
proxyInclude=$(shell find proxy -type f)

$(shell curl -o "wordpress/bjtj.zip" "https://raw.githubusercontent.com/bohendo/bjtj/master/build/bjtj.zip")

##### RULES #####

all: proxy-image wordpress-image
	@true

deploy: proxy-image wordpress-image
	docker push $(me)/blog_proxy:$v
	docker push $(me)/blog_wordpress:$v

.makeflags/proxy-image: $(proxyInclude)
	docker build -f proxy/Dockerfile -t $(me)/blog_proxy:$v .
	touch .makeflags/proxy-image

.makeflags/wordpress-image: $(wordpressInclude)
	docker build -f wordpress/Dockerfile -t $(me)/blog_wordpress:$v .
	touch .makeflags/wordpress-image
