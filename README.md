
# Nginx/WordPress Blog

This repo contains the machinery required to host a personal blog aka two Docker containers:

 - **`blog_proxy`: Nginx & Certbot**: This is the container communicating w the rest of the world on ports 80 & 443. This container has both nginx and certbot installed where nginx is the primary process. Certbot is used to initialize certs for our HTTPS connection before Nginx starts. While Nginx is running, a secondary certbot process in the background will periodically perform a cert-renewal check.

 - **`blog_wordpress`: Apache & PHP**: This container's primary process is Apache & it serves the WordPress core PHP files. These PHP files are intended to be made persistent in a docker volume mounted to /var/www/wordpress so your plugins will be preserved across reboots & easily backed up. It comes prepackaged with an experimental Blackjack Tip Jar plugin instead of the usual defaults: Hello Dolly & Akismet.

The blog machine also relies on a MySQL database but we'll use a pre-built default image, no need for us to re-invent this wheel.

To use these containers for your own purposes, you have one or two steps: build (optional) & deploy.

## Build: `make deploy`

**Make sure you have git, make, and docker installed on the machine where you build.**

Building is optional: I've already built these containers and pushed them to DockerHub. You can use these (see deploy section) if you don't want to build your own.

To build, this is the only command you need: `make deploy`. This does two important things, it runs a couple `docker build` commands to build the two containers described above and then it pushes these images to DockerHub. To run `make deploy`, you need to have a DockerHub account you can push to. This probably means running `docker login`.

Side note: You can also run `make` to build the containers locally without pushing to DockerHub.

Makefile uses the `whoami` command to get your username & uses that as the default DockerHub username while pushing. If your DockerHub username is different that your machine's username, change this value at the top of the Makefile to suit your situation.

All in all, it'll look something like this:

```bash
# login to DockerHub if you haven't already
docker login

git clone https://github.com/bohendo/blog.git
cd blog

# change the "me" variable at the top of this file
# if your docker id is different than `whoami`
vim Makefile 

# build docker images & push them to DockerHub
make deploy
```

You have now built & saved 2 images to DockerHub, they are ready & waiting to be deployed **anywhere** that has an internet connection & docker installed.

## Deploy: `bash ops/deploy.sh`

Make sure you have **docker** installed on the machine where you deploy.

When deploying, this system looks for two environment variables:

 - `DOMAINNAME`: Specifies the domain name we're deploying our blog to. This is what our proxy container's certbot will try to get certificates for.
 - `EMAIL`: If our certificates are about to expire, a reminder email will be sent to the value contained here

The deploy script is basically a `docker-compose.yml` file wrapped into a bash script. It'll initialize some Docker secrets and bind-mounts before creating & deploying the docker compose file. It will run `docker pull $image` where `$image` is something like `bohendo/blog_wordpress:latest`. It pulls the username from the `whoami` command so make sure to change it to fit your situation.

**If you didn't build your own, change the username to `bohendo` to pull the images I use in production.**

Deploying locally will look something like this:

```bash
# change the "me" variable at the top of this file
# if your docker id is different than `whoami`
vim ops/deploy.sh

# set some environment variables
export DOMAINNAME=example.com
export EMAIL=myself@example.com

bash ops/deploy.sh
```

And deploying to a server you have ssh access to will look something like this:

```bash
# Give this script to some remote machine
scp ops/deploy.sh myServer:~/deploy-blog.sh

# Execute this script on our remote machine with some env vars
ssh myServer DOMAINNAME=example.com EMAIL=test@example.com bash ~/deploy-blog.sh
```

And you're good to go! The deploy script will initialize all your secrets for you & deploy the project with an HTTPS connection to the `DOMAINNAME`. Try visiting from a web browser to login to WordPress and finish setting up your new blog.

## Feedback is my favorite

If anything is broken or confusing, please open an issue or tweet @bohendo and I'll do my best to help out.

If you notice any security vulnerabilities, then definitely open an issue or tweet at me. No formal bounty programs are in place but, if you find something really cool, I might still throw a little ETH at you to say thanks.

