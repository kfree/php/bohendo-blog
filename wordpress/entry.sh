#!/bin/bash

env

echo "WP Entry Activated! Waiting for MySQL to come alive..."
while true
do

  php <<-EOF
  <?php
    error_reporting(0);
    \$conn = new mysqli(
      "${WORDPRESS_DB_HOST%:*}",
      "$WORDPRESS_DB_USER",
      file_get_contents("$WORDPRESS_DB_PASSWORD_FILE"),
      "$WORDPRESS_DB_NAME"
    );
    if (\$conn->connect_error) {
        printf(\$conn->connect_error);
        exit(1);
    }
    exit(2);
  ?>
	EOF

  # we use exit code 2 for success; php parsing error results in exit code 0
  if [[ "$?" == "2" ]]
  then
    echo "Successfully connected to MySQL"

    # Load a new wordpress core if it's not already there
    # -nvr for no-clobber verbose recursively
    cp -nvr /root/wordpress/* /var/www/wordpress/
    cp -nv /root/wordpress/.htaccess /var/www/wordpress/.htaccess
    cp -fvr /root/wordpress/wp-content/plugins/bjtj /var/www/wordpress/wp-content/plugins/

    chown -R www-data:www-data /var/www/wordpress

    echo "Entry complete, executing Apache";
    exec /usr/sbin/apache2ctl -D FOREGROUND

  else
    echo # flush stdout to show php output
    sleep 5
  fi

done
