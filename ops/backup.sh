#!/bin/bash

if [[ -z "`docker service ps -q blog_mysql`" ]]
then
  echo "Error: expected to see a blog_mysql service running"
  exit 1
fi

# Setup environment
set -e
sudo true
me=`whoami`
bn=blog_backup # basename of backup dir & tarball
d="/tmp/$bn" # directory where we'll put stuff to backup

# Reset & init backup directory
[[ -d "$d" ]] && rm -rf $d
mkdir -p $d

echo -n "Backing up database..."

# Backup database contents
id=`for f in $(docker service ps -q blog_mysql)
do
  docker inspect --format '{{.Status.ContainerStatus.ContainerID}}' $f
done | head -n1`

docker exec $id bash -c 'mysqldump -u wordpress -p`cat /run/secrets/wp_mysql` wordpress' > $d/$bn.sql 2> /dev/null

echo "Done"; echo -n "Copying PHP plugins..."
sudo cp -r /var/lib/docker/volumes/blog_wp_data $d

echo "Done"; echo -n "Copying SSL certs..."
sudo cp -r /var/lib/docker/volumes/blog_letsencrypt $d

echo "Done"; echo -n "Creating a zipped tarball..."

sudo chown -R $me:$me $d
sudo tar -C /tmp -czf $bn.tar.gz $bn
sudo chown $me:$me $bn.tar.gz

echo "Done"; echo -n "Cleaning up..."

rm -rf $d

echo "Done"
